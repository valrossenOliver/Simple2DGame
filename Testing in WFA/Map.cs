﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Testing_in_WFA
{

    class Map
    {
        //Variables
        public string name;
        public List<Tile> tileList;
        public int width, height;
        public Point Homebase;

        public static List<Map> list = new List<Map>();

        public Map(int _width, int _height, int[] _tileList, int[] _objectList, bool loaded = false, string _name = "")
        {
            //Om banan inte laddats tidigare (laddats ifrån fil) ge den ett nytt namn. (OBS!!!!! DETTA BÖR INTE HÄNDA OM NYA SYSTEMED ANVÄNDS)
            if (!(loaded)) { list.Add(this); name = "level" + list.ToArray().Length.ToString(); }
            else
            {
                //Om information skickades, kommer inte namn genereras, utan ges ifrån filen.
                name = _name;
                width = _width;
                height = _height;

                //Generera en lista av alla tiles som finns på kartan.
                generateTileList(_tileList, _objectList);
            }

            // TODO - Ge varje karta en egen spawnpoint. Denna skall bara synlig som en flagga på kartan.
            Player.posX = Homebase.X;
            Player.posY = Homebase.Y;


                //===========================================================================================================================\\
               // Player.inventory.Clear(); Detta är onödigt, borde bara hända om ett nytt spelare-objekt behövs. Spelaren kan ju byta bana.  \\
              //                                                                                                                               \\
             //                                              TL;DR Kod kan nog tags bort                                                        \\
            //===================================================================================================================================\\
        }

        public static Map loadFile (string _mapName)
        {
            //Skapa en filström för att läsa av filen.
            FileStream file = new FileStream("res/" + _mapName + ".xml", FileMode.Open);
            //Ladda filen med ett xDoc-Dokument.
            XDocument xDoc = XDocument.Load(file);

            //Skapar viarabler för att hantera element.
            int width = 0, height = 0;
            List<int> tlist = new List<int>(), olist = new List<int>(); //[tileList, objektList]

            //Försöker att ladda all information.
            try
            {
                //Kollar varje karta sparat i dokumentet (alltid ett)
                foreach (var map in xDoc.Descendants("Map"))
                {
                    //Kollar om kartans namn stämmer överens med banan som sökes, och stämmer överens med filnamnet.
                    if (map.Attribute("name").Value == _mapName)
                    {
                        //Sparar storlek på kartan.
                        width = int.Parse(map.Attribute("width").Value);
                        height = int.Parse(map.Attribute("height").Value);

                        //Sparar alla tiles som finns på kartan i filen.
                        var TILES = map.Descendants("Tile");

                        //Kollar igenom varje tile som finns.
                        foreach (var tile in TILES)
                        {
                            //Kollar vilken sorts tileID det är och lägger till det tileID:t i listan.

                            /* TODO - Borde skapa en lista eller liknande för alla tillängliga tile-typer.
                             * Fler tile-sorter skulle göra laddningkoden mycket lång och svår att kolla.
                             * Alternativt kan man tag bort kollen, och utgå ifrån att inte XML-Dokumentet har redigerats manuellt.
                            */
                            if (tile.Element("TileType").Value == tileType.Grass.ToString())
                            {
                                tlist.Add((int)tileType.Grass);
                            }
                            else if (tile.Element("TileType").Value == tileType.Water.ToString())
                            {
                                tlist.Add((int)tileType.Water);
                            }
                            else if (tile.Element("TileType").Value == tileType.Bridge.ToString())
                            {
                                tlist.Add((int)tileType.Bridge);
                            }
                            else
                            {
                                //Om det inte matchar något ID, spara 
                                tlist.Add(0);
                            }

                            //Sparar objekt som finns i den tilen.
                            var interactable = tile.Element("Interactable");
                            olist.Add(int.Parse(interactable.Attribute("ID").Value));
                        }
                        //Om banan stämde kan loopen avbrytas. (OBS!!! Detta hade hänt ändå, det finns bara ett map-element i filen)
                        break;
                    }
                }
            }
            catch //Om laddning misslyckades...
            {
                //.. visa popup-ruta för att banan inte kunde laddas.
                MessageBox.Show("Could not load map... :(", "ERROR");
            }

            //Stänger och kastar filströmmen.
            file.Close();
            file.Dispose();

            return (new Map(width, height, tlist.ToArray(), olist.ToArray(), true, _mapName));
        }

        public void generateTileList(int[] _tileList, int[] _objectList = null)
        {
            //Skapar en ny tileList
            tileList = new List<Tile>(height * width);

            //Går igenom loop like många gånger som tiles behövs.
            for (int i = 0; i < _tileList.Length; i++)
            {
                //Kollar om det skall finnas ett objekt på tilen.
                if (_objectList != null)
                {
                    //Kollar så att objektet inte är nil (vid laddning av fil)
                    if (_objectList[i] != 0)
                    {
                        //Lägger till tile med objekt på.
                        tileList.Add(new Tile(_tileList[i], _objectList[i]));

                        //Avbryter denna loopomgånen och kör nästa loop.
                        continue;
                    }
                }

               //Lägger till den tile som skall vara på denna platsen [ID "i"]. (OBS!!!! Detta händer inte om en tile redan lagts till tidigare)
               tileList.Add(new Tile(_tileList[i]));
            }

            //Sätter spawnpoint (om det finns)
            for (int i = 0; i < tileList.Count; i++)
            {
                if (tileList[i].interactable.name == "Homebase")
                {
                    Homebase = getTileCoordinates(i);
                }
            }

        }

        //Rita kartan på canvas.
        public void draw(Panel canvas, int level = 0)
        {
            //OBS!!!! Level betyder på vilken nivå som canvas behöver uppdateras.

            using (Graphics graphics = canvas.CreateGraphics())
            {
                //Skapar bilder för varje layer.
                Image backgroundLayer = canvas.BackgroundImage;
                if (backgroundLayer == null)
                {
                    backgroundLayer = new Bitmap(800, 600);
                }
                Image objectLayer = new Bitmap(800, 600);
                Image gui = GUI.generate();

                //Skapar grafisk ritare för varje layer.
                Graphics layer0 = Graphics.FromImage(backgroundLayer);
                Graphics layer1 = Graphics.FromImage(objectLayer);

                //Generera karta (om max layer 0)
                if (level <= 0)
                {
                    for (int row = 0; row < height; row++)
                    {
                        for (int tile = 0; tile < width; tile++)
                        {
                            layer0.DrawImage(tileList[row * width + tile].texture, 40 * tile, 40 * row, 40, 40);

                            if (0 < row)
                            {
                                if (tileList[row * width + tile].name == "void" && tileList[(row - 1) * width + tile].subTexture != null)
                                {
                                    layer0.DrawImage(tileList[(row - 1) * width + tile].subTexture, 40 * tile, 40 * row, 40, 40);
                                }
                            }

                            //Ritar ID på varje tile (DEBUG)
                            if (MainFrame.DEBUG)
                            {
                                layer0.DrawString((row * width + tile).ToString(), MainFrame.DefaultFont, new SolidBrush(Color.Black), 40 * tile, 40 * row);
                            }
                        }
                    }
                }

                //Generera objekt (om max layer 1)
                if (level <= 1)
                {
                    for (int row = 0; row < height; row++)
                    {
                        for (int tile = 0; tile < width; tile++)
                        {
                            if (tileList[row * width + tile].interactable.texture != null)
                            {
                                layer1.DrawImage(tileList[row * width + tile].interactable.texture, 40 * tile, 40 * row, 40, 40);
                            }
                        }
                    }

                }

                //Sparar senaste banans uppdatering för att inte behöva skapa nya bitmaps.
                if (canvas.BackgroundImage != backgroundLayer)
                {
                    canvas.BackgroundImage = backgroundLayer;
                }

                //Slår ihop alla lager i ett nytt.
                Image finalDrawing = new Bitmap(800, 600);
                Graphics pensil = Graphics.FromImage(finalDrawing);

                pensil.DrawImage(backgroundLayer, 0, 0); // Lager 0 - Karta
                pensil.DrawImage(objectLayer, 0, 0);     // Lager 1 - Objekt

                //Ritar ut spelaren. (Blivande lager 2, karaktärer)
                pensil.DrawImage(Player.characterSprite, 40 * Player.posX, 40 * Player.posY, 40, 40);

                //Ritar GUI
                pensil.DrawImage(gui, 0, 0);

                //Ritar ut resultatet på canvas.
                graphics.DrawImage(finalDrawing, 0, 0, 800, 600);

                //Gör av med alla element för att förebygga memory leaks.
                layer0.Dispose();
                layer1.Dispose();
                pensil.Dispose();

                finalDrawing.Dispose();
                objectLayer.Dispose();
                gui.Dispose();
            }

        }

        public Tile getTile(int x, int y)
        {
            //Returnerar den tile som ligger vid (x, y)
            return (tileList[y*width+x]);
        }

        public int getTileID(int x, int y)
        {
            //Returnerar ID på dne tile som ligger vid (x, y)
            return (y * width + x);
        }

        public Point getTileCoordinates(int ID)
        {
            int y = 0, x = 0;

            while (ID % 20 != 0)
            {
                ID--;
                x++;
            }

            y = ID / 20;

            return (new Point(x, y));
        }

        public List<Tile> getAdjacentTiles(int target)
        {
            //Skapa en lista av alla tiles som kan ligga bredvid target.
            List<Tile> adjacents = new List<Tile>(4);

            //Kollar om target har en höger.
            if ((target % 20) != 19)
            {
                adjacents.Add(tileList[target + 1]);
            }
            else
            {
                adjacents.Add(new Tile(0));
            }

            //Kollar om target har en ned.
            if (target < width * height - width)
            {
                //Lägger till TILE nedåt.
                adjacents.Add(tileList[target + width]);
            }
            else
            {
                adjacents.Add(new Tile(0));
            }

            //Kollar om target har en vänster.
            if (target % width != 0)
            {
                //Lägger till TILE till vänster.
                adjacents.Add(tileList[target - 1]);
            }
            else
            {
                adjacents.Add(new Tile(0));
            }

            //Kollar om target har en upp.
            if (width <= target)
            {
                //Lägger till TILE uppåt.
                adjacents.Add(tileList[target - width]);
            }
            else
            {
                adjacents.Add(new Tile(0));
            }

            //Returnera alla grannar
            return adjacents;
        }

        /*int counts = 0;
        //Letar upp snabbaste vägen till en plats.
        public List<int> findPath(int start, int target, bool isStart = true, int maxTiles = 0, List<int> currentPath = null, List<List<int>> possiblePaths = null)
        {
            //Kollar om detta är starten eller under en räkning.
            if (isStart)
            {
                MessageBox.Show("Search stared.");
                currentPath = new List<int>();
                possiblePaths = new List<List<int>>();
            }

            counts++;
            if (counts == 20)
            {
                MessageBox.Show("Paths " + possiblePaths.Count + 1 + "\nPath length: " + currentPath.Count + "\nCurrent tile: " + start);
                counts = 0;
            }

            if (tileList[start].isPassable())
            {
                if (!isStart)
                    currentPath.Add(start);
            }
            else
            {
                return currentPath;
            }


            if (maxTiles != 0 && maxTiles < currentPath.Count)
            {
                currentPath.RemoveAt(currentPath.Count - 1);

                return currentPath;
            }

            if (start == target)
            {
                MessageBox.Show("Paths " + possiblePaths.Count + 1 + "\nPath length: " + currentPath.Count + "\nCurrent tile: " + start);

                possiblePaths.Add(new List<int>(currentPath));

                currentPath.RemoveAt(currentPath.Count - 1);

                return currentPath;
            }

            //Sparar vilka tiles man kan gå till ifrån nuvarande tile
            List<Tile> adjacentTiles = getAdjacentTiles(start);

            //Kollar varje håll efter target.
            if (adjacentTiles[0].name != "void" && !currentPath.Contains(start + 1)) //Höger
            {
                currentPath = findPath(start + 1, target, false, maxTiles, currentPath, possiblePaths);
            }
            if (adjacentTiles[1].name != "void" && !currentPath.Contains(start + 20)) //Ned
            {
                currentPath = findPath(start + 20, target, false, maxTiles, currentPath, possiblePaths);
            }
            if (adjacentTiles[2].name != "void" && !currentPath.Contains(start - 1)) //Vänster
            {
                currentPath = findPath(start - 1, target, false, maxTiles, currentPath, possiblePaths);
            }
            if (adjacentTiles[3].name != "void" && !currentPath.Contains(start - 20)) //Upp
            {
                currentPath = findPath(start - 20, target, false, maxTiles, currentPath, possiblePaths);
            }


            if (isStart)
            {
                MessageBox.Show("All possible paths calculated. (" + possiblePaths.Count + ")");
                for (int i = 1; i < possiblePaths.Count;)
                {
                    if (possiblePaths[i].Count < currentPath.Count)
                    {
                        possiblePaths.Remove(currentPath);

                        currentPath = possiblePaths[i];
                    }
                    else if (currentPath.Count < possiblePaths[i].Count)
                    {
                        possiblePaths.RemoveAt(i);
                    }
                }
                MessageBox.Show("Paths generated.");
            }
            else
            {
                currentPath.RemoveAt(currentPath.Count - 1);
            }

            return currentPath;
        }*/
    }
}
