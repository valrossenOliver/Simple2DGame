﻿using System.Drawing;

namespace Testing_in_WFA
{

    public enum tileType { Grass = 1, Water = 2, Bridge = 3 }

    class Tile
    {
        //Varables
        public string name, info;
        public Image texture = null, subTexture = null;
        public int typeID;
        bool trasspassable = true;
        public TileObject interactable = TileObject.tileObjectList[0];


        public Tile(int i, int o = 0)
        {
            generateInfo(i);

            if (o != 0)
            {
                interactable = TileObject.tileObjectList[o];
            }

        }

        public bool isPassable()
        {
            return trasspassable;
        }

        public void removeTileObject ()
        {
            interactable = TileObject.tileObjectList[0];
        }

        public void generateInfo(int i)
        {
            typeID = i;

            switch (i)
            {
                case (int)tileType.Grass:
                    texture = new Bitmap("src/grass.png");
                    subTexture = new Bitmap("src/grass_subtexture.png");
                    name = "Grass";
                    info = "The grass is green.";
                    trasspassable = true;
                    break;
                case (int)tileType.Water:
                    texture = new Bitmap("src/water.png");
                    name = "Water";
                    info = "There is no swim-function...";
                    trasspassable = false;
                    break;
                case (int)tileType.Bridge:
                    texture = new Bitmap("src/bridge.png");
                    name = "Bridge";
                    info = "This is a wooden bridge.";
                    trasspassable = true;
                    break;
                default:
                    texture = new Bitmap("src/void.png");
                    name = "void";
                    info = "The void is empty. Just like your friendslist!";
                    trasspassable = false;
                    break;
            }
        }

    }
}
