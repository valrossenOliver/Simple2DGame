﻿using System.Drawing;

namespace Testing_in_WFA
{
    static class GUI
    {
        static Map currentMap;
        public static bool inventory_Open = false;

        public static void setMap(Map _map)
        {
            //Byter nuvarande bana.
            currentMap = _map;
        }

        public static void inventory()
        {
            //Byter inventarie mellan synligt / osynligt.
            inventory_Open = !inventory_Open;
        }

        public static Image generate()
        {
            //Skapar en bild att returnera (alla texturer kommer liggar på denna)
            Bitmap textures = new Bitmap(800, 600);           

            //Skapar en grafisk ritare för texturer och en för text.
            Graphics gui = Graphics.FromImage(textures);
            Graphics guiTEXT = Graphics.FromImage(textures);

            //Sätter hur text skall genereras. (Så at den inte blir suddig)
            guiTEXT.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;

            //Generera interaktionssymbol vid spelaren.
            if (currentMap.getTile(Player.posX, Player.posY).interactable != TileObject.tileObjectList[0])
            {
                using (Bitmap reactionTexture = new Bitmap("src/reaction_explemation.png"))
                {
                    gui.DrawImage(reactionTexture, Player.posX * 40, Player.posY * 40 - 40, 40, 40);
                    reactionTexture.Dispose();
                }
            }

            //TODO - Generera interaktionssymbol för möjlig interaktion med objekt. (Typ bygga bro om man har stickstack)
            foreach (Tile t in currentMap.getAdjacentTiles(currentMap.getTileID(Player.posX, Player.posY)))
            {
                if (t.interactable != TileObject.tileObjectList[0])
                {
                    using (Bitmap reactionTexture = new Bitmap("src/reaction_questionmark.png"))
                    {
                        gui.DrawImage(reactionTexture, Player.posX * 40, Player.posY * 40 - 40, 40, 40);
                        reactionTexture.Dispose();
                    }
                }
            }

            //Rita inventarie.
            if (inventory_Open)
            {
                //
                SolidBrush brush = new SolidBrush(Color.FromArgb(220, 25, 25, 25));
                Pen pen = new Pen(Color.White);

                gui.FillRectangle(brush, Player.posX * 40 + 60, Player.posY * 40, 40 * 3, 40 * 3 + 20);

                gui.DrawRectangle(pen, Player.posX * 40 + 80, Player.posY * 40 + 40, 40, 40);
                gui.DrawRectangle(pen, Player.posX * 40 + 120, Player.posY * 40 + 40, 40, 40);
                gui.DrawRectangle(pen, Player.posX * 40 + 80, Player.posY * 40 + 80, 40, 40);
                gui.DrawRectangle(pen, Player.posX * 40 + 120, Player.posY * 40 + 80, 40, 40);

                brush = new SolidBrush(Color.White);

                guiTEXT.DrawString("Inventory", new Font("Consolas", 12, FontStyle.Regular), brush, Player.posX * 40 + 76, Player.posY * 40 + 5);

                int i = 1;
                foreach (inventoryObject io in Player.inventory)
                {
                    if (io != null)
                    {
                        if (i == 1)
                            gui.DrawImage(io.texture, Player.posX * 40 + 80, Player.posY * 40 + 40, 40, 40);
                        if (i == 2)
                            gui.DrawImage(io.texture, Player.posX * 40 + 120, Player.posY * 40 + 40, 40, 40);
                        if (i == 3)
                            gui.DrawImage(io.texture, Player.posX * 40 + 80, Player.posY * 40 + 80, 40, 40);
                        if (i == 4)
                            gui.DrawImage(io.texture, Player.posX * 40 + 120, Player.posY * 40 + 80, 40, 40);
                    }

                    i++;
                }

                pen.Dispose();
                brush.Dispose();
            }
            
            gui.Dispose();
            guiTEXT.Dispose();

            return textures;
        }

    }
}
