﻿using System.Drawing;
using System.Collections.Generic;

namespace Testing_in_WFA
{
    class TileObject
    {
        public static List<TileObject> tileObjectList = new List<TileObject>();

        //Variables
        public string name;
        public string desc;
        public Image texture;
        inventoryObject item;
        public bool trasspassable = false, pickup = false;

        public TileObject (string _name, string _desc, Image _texture = null, bool _pickup = false, inventoryObject i = null) {
            name = _name;
            desc = _desc;
            pickup = _pickup;
            item = i; //new inventoryObject(i.name, i.desc, i.texture, i.targetType) KAN BEHÖVAS VID REMOVAL

            if (_texture != null)
                texture = _texture;

            if (!(tileObjectList.Contains(this)))
                tileObjectList.Add(this);
        }

        public inventoryObject convert()
        {
            //Returnera inventoryObject som går att omvandla till.
            if (item != null && pickup == true)
                return (new inventoryObject(item.name, item.desc, item.texture, item.targetType));

            return null;
        }

        public bool isPassable()
        {
            //Returnera om spelaren kan gå på denna tilen eller inte.
            return trasspassable;
        }
    }
}
