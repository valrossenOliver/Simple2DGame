﻿namespace Testing_in_WFA
{
    partial class MainFrame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.canvas = new System.Windows.Forms.Panel();
            this.btnDraw = new System.Windows.Forms.Button();
            this.isNone = new System.Windows.Forms.RadioButton();
            this.isBlue = new System.Windows.Forms.RadioButton();
            this.isGreen = new System.Windows.Forms.RadioButton();
            this.numY = new System.Windows.Forms.NumericUpDown();
            this.numX = new System.Windows.Forms.NumericUpDown();
            this.lblX = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.lblWaterWarning = new System.Windows.Forms.Label();
            this.lblWorldEditor = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMapSaveLoadInfo = new System.Windows.Forms.Label();
            this.txtBoxTargetMap = new System.Windows.Forms.TextBox();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnInteract = new System.Windows.Forms.Button();
            this.txtBoxUpdates = new System.Windows.Forms.TextBox();
            this.btnInventory = new System.Windows.Forms.Button();
            this.btnChoiceNo = new System.Windows.Forms.Button();
            this.btnChoiceYes = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numX)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // canvas
            // 
            this.canvas.BackColor = System.Drawing.Color.Transparent;
            this.canvas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.canvas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canvas.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.canvas.ForeColor = System.Drawing.Color.Transparent;
            this.canvas.Location = new System.Drawing.Point(390, 40);
            this.canvas.Name = "canvas";
            this.canvas.Size = new System.Drawing.Size(800, 600);
            this.canvas.TabIndex = 0;
            this.canvas.Click += new System.EventHandler(this.canvas_MouseClick);
            this.canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
            // 
            // btnDraw
            // 
            this.btnDraw.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDraw.Location = new System.Drawing.Point(5, 172);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(184, 38);
            this.btnDraw.TabIndex = 1;
            this.btnDraw.Text = "Edit Square";
            this.btnDraw.UseVisualStyleBackColor = true;
            this.btnDraw.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // isNone
            // 
            this.isNone.AutoSize = true;
            this.isNone.ForeColor = System.Drawing.Color.Black;
            this.isNone.Location = new System.Drawing.Point(70, 149);
            this.isNone.Name = "isNone";
            this.isNone.Size = new System.Drawing.Size(51, 17);
            this.isNone.TabIndex = 2;
            this.isNone.Text = "None";
            this.isNone.UseVisualStyleBackColor = true;
            // 
            // isBlue
            // 
            this.isBlue.AutoSize = true;
            this.isBlue.ForeColor = System.Drawing.Color.Black;
            this.isBlue.Location = new System.Drawing.Point(70, 126);
            this.isBlue.Name = "isBlue";
            this.isBlue.Size = new System.Drawing.Size(46, 17);
            this.isBlue.TabIndex = 3;
            this.isBlue.Text = "Blue";
            this.isBlue.UseVisualStyleBackColor = true;
            // 
            // isGreen
            // 
            this.isGreen.AutoSize = true;
            this.isGreen.Checked = true;
            this.isGreen.ForeColor = System.Drawing.Color.Black;
            this.isGreen.Location = new System.Drawing.Point(70, 103);
            this.isGreen.Name = "isGreen";
            this.isGreen.Size = new System.Drawing.Size(54, 17);
            this.isGreen.TabIndex = 4;
            this.isGreen.TabStop = true;
            this.isGreen.Text = "Green";
            this.isGreen.UseVisualStyleBackColor = true;
            // 
            // numY
            // 
            this.numY.Location = new System.Drawing.Point(58, 77);
            this.numY.Maximum = new decimal(new int[] {
            14,
            0,
            0,
            0});
            this.numY.Name = "numY";
            this.numY.Size = new System.Drawing.Size(107, 20);
            this.numY.TabIndex = 5;
            // 
            // numX
            // 
            this.numX.Location = new System.Drawing.Point(58, 51);
            this.numX.Maximum = new decimal(new int[] {
            19,
            0,
            0,
            0});
            this.numX.Name = "numX";
            this.numX.Size = new System.Drawing.Size(107, 20);
            this.numX.TabIndex = 6;
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Location = new System.Drawing.Point(2, 53);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(50, 13);
            this.lblX.TabIndex = 7;
            this.lblX.Text = "X (0 - 19)";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Location = new System.Drawing.Point(2, 79);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(50, 13);
            this.lblY.TabIndex = 8;
            this.lblY.Text = "Y (0 - 14)";
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(128, 108);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(75, 23);
            this.btnDown.TabIndex = 9;
            this.btnDown.Text = "Down";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(184, 74);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(75, 23);
            this.btnRight.TabIndex = 10;
            this.btnRight.Text = "Right";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(81, 74);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(75, 23);
            this.btnLeft.TabIndex = 11;
            this.btnLeft.Text = "Left";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnUp
            // 
            this.btnUp.Location = new System.Drawing.Point(128, 40);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(75, 23);
            this.btnUp.TabIndex = 12;
            this.btnUp.Text = "Up";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // lblWaterWarning
            // 
            this.lblWaterWarning.AutoSize = true;
            this.lblWaterWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWaterWarning.ForeColor = System.Drawing.Color.Red;
            this.lblWaterWarning.Location = new System.Drawing.Point(612, 6);
            this.lblWaterWarning.Name = "lblWaterWarning";
            this.lblWaterWarning.Size = new System.Drawing.Size(365, 31);
            this.lblWaterWarning.TabIndex = 13;
            this.lblWaterWarning.Text = "You cannot traspass that tile!";
            this.lblWaterWarning.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblWaterWarning.Visible = false;
            // 
            // lblWorldEditor
            // 
            this.lblWorldEditor.AutoSize = true;
            this.lblWorldEditor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorldEditor.Location = new System.Drawing.Point(66, 7);
            this.lblWorldEditor.Name = "lblWorldEditor";
            this.lblWorldEditor.Size = new System.Drawing.Size(96, 20);
            this.lblWorldEditor.TabIndex = 14;
            this.lblWorldEditor.Text = "World Editor";
            this.lblWorldEditor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Controls.Add(this.lblMapSaveLoadInfo);
            this.panel1.Controls.Add(this.txtBoxTargetMap);
            this.panel1.Controls.Add(this.btnExport);
            this.panel1.Controls.Add(this.btnImport);
            this.panel1.Controls.Add(this.lblWorldEditor);
            this.panel1.Controls.Add(this.numX);
            this.panel1.Controls.Add(this.numY);
            this.panel1.Controls.Add(this.lblX);
            this.panel1.Controls.Add(this.lblY);
            this.panel1.Controls.Add(this.isGreen);
            this.panel1.Controls.Add(this.btnDraw);
            this.panel1.Controls.Add(this.isNone);
            this.panel1.Controls.Add(this.isBlue);
            this.panel1.Location = new System.Drawing.Point(12, 426);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 214);
            this.panel1.TabIndex = 15;
            // 
            // lblMapSaveLoadInfo
            // 
            this.lblMapSaveLoadInfo.AutoSize = true;
            this.lblMapSaveLoadInfo.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMapSaveLoadInfo.Location = new System.Drawing.Point(236, 83);
            this.lblMapSaveLoadInfo.Name = "lblMapSaveLoadInfo";
            this.lblMapSaveLoadInfo.Size = new System.Drawing.Size(80, 17);
            this.lblMapSaveLoadInfo.TabIndex = 18;
            this.lblMapSaveLoadInfo.Text = "Map Name:";
            // 
            // txtBoxTargetMap
            // 
            this.txtBoxTargetMap.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txtBoxTargetMap.Location = new System.Drawing.Point(195, 103);
            this.txtBoxTargetMap.MaxLength = 24;
            this.txtBoxTargetMap.Name = "txtBoxTargetMap";
            this.txtBoxTargetMap.Size = new System.Drawing.Size(158, 20);
            this.txtBoxTargetMap.TabIndex = 17;
            this.txtBoxTargetMap.Text = "level1";
            this.txtBoxTargetMap.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnExport
            // 
            this.btnExport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnExport.Location = new System.Drawing.Point(195, 128);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(158, 38);
            this.btnExport.TabIndex = 16;
            this.btnExport.Text = "Export Level";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnImport
            // 
            this.btnImport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnImport.Location = new System.Drawing.Point(195, 172);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(158, 38);
            this.btnImport.TabIndex = 15;
            this.btnImport.Text = "Import Level";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnInteract
            // 
            this.btnInteract.Location = new System.Drawing.Point(265, 40);
            this.btnInteract.Name = "btnInteract";
            this.btnInteract.Size = new System.Drawing.Size(103, 125);
            this.btnInteract.TabIndex = 16;
            this.btnInteract.Text = "Interact with environment\r\n( ! )\r\n";
            this.btnInteract.UseVisualStyleBackColor = true;
            this.btnInteract.Click += new System.EventHandler(this.btnInteract_Click);
            // 
            // txtBoxUpdates
            // 
            this.txtBoxUpdates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxUpdates.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxUpdates.Location = new System.Drawing.Point(12, 174);
            this.txtBoxUpdates.Multiline = true;
            this.txtBoxUpdates.Name = "txtBoxUpdates";
            this.txtBoxUpdates.ReadOnly = true;
            this.txtBoxUpdates.Size = new System.Drawing.Size(356, 217);
            this.txtBoxUpdates.TabIndex = 17;
            this.txtBoxUpdates.Text = "Lorem ipsum dolor sit amet...";
            // 
            // btnInventory
            // 
            this.btnInventory.Location = new System.Drawing.Point(12, 40);
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Size = new System.Drawing.Size(63, 125);
            this.btnInventory.TabIndex = 18;
            this.btnInventory.Text = "Inventory";
            this.btnInventory.UseVisualStyleBackColor = true;
            this.btnInventory.Click += new System.EventHandler(this.bntInventory_Click);
            // 
            // btnChoiceNo
            // 
            this.btnChoiceNo.Enabled = false;
            this.btnChoiceNo.Location = new System.Drawing.Point(18, 397);
            this.btnChoiceNo.Name = "btnChoiceNo";
            this.btnChoiceNo.Size = new System.Drawing.Size(168, 23);
            this.btnChoiceNo.TabIndex = 19;
            this.btnChoiceNo.Text = "No";
            this.btnChoiceNo.UseVisualStyleBackColor = true;
            this.btnChoiceNo.Click += new System.EventHandler(this.btnChoiceNo_Click);
            // 
            // btnChoiceYes
            // 
            this.btnChoiceYes.Enabled = false;
            this.btnChoiceYes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChoiceYes.Location = new System.Drawing.Point(192, 397);
            this.btnChoiceYes.Name = "btnChoiceYes";
            this.btnChoiceYes.Size = new System.Drawing.Size(164, 23);
            this.btnChoiceYes.TabIndex = 20;
            this.btnChoiceYes.Text = "Yes";
            this.btnChoiceYes.UseVisualStyleBackColor = true;
            this.btnChoiceYes.Click += new System.EventHandler(this.btnChoiceYes_Click);
            // 
            // MainFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.btnChoiceYes);
            this.Controls.Add(this.btnChoiceNo);
            this.Controls.Add(this.btnInventory);
            this.Controls.Add(this.txtBoxUpdates);
            this.Controls.Add(this.btnInteract);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblWaterWarning);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.canvas);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainFrame";
            this.Text = "Main Frame";
            ((System.ComponentModel.ISupportInitialize)(this.numY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numX)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel canvas;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.RadioButton isNone;
        private System.Windows.Forms.RadioButton isBlue;
        private System.Windows.Forms.RadioButton isGreen;
        private System.Windows.Forms.NumericUpDown numY;
        private System.Windows.Forms.NumericUpDown numX;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Label lblWaterWarning;
        private System.Windows.Forms.Label lblWorldEditor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnInteract;
        private System.Windows.Forms.TextBox txtBoxUpdates;
        private System.Windows.Forms.Button btnInventory;
        private System.Windows.Forms.Button btnChoiceNo;
        private System.Windows.Forms.Button btnChoiceYes;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Label lblMapSaveLoadInfo;
        private System.Windows.Forms.TextBox txtBoxTargetMap;
    }
}

