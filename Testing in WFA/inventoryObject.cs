﻿using System.Drawing;

namespace Testing_in_WFA
{
    class inventoryObject
    {
        public enum eTargetType { GrassTile, WaterTile, BridgeTile }

        //Variabels
        public string name, desc;
        public Image texture;
        public eTargetType targetType;

        public inventoryObject (string _name, string _desc, Image _texture, eTargetType tt)
        {
            name = _name;
            desc = _desc;
            texture = _texture;

            targetType = tt;
        }
    }
}
