﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing_in_WFA
{
    static class Player
    {
        public enum direction { Right = -90, Down = 0, Left = 90, Up = 180 };

        //Variabler
        public static int posX = 0, posY = 0;
        public static List<inventoryObject> inventory = new List<inventoryObject>(4)
        {
            null, null, null, null
        };
        public static Image defaultSprite = new Bitmap("src/Player.png");
        public static Image characterSprite = new Bitmap("src/Player.png");
        public static direction facingDirection = direction.Down;

        public static bool onField = true;

        //Omvandla tileObject till unventoryObject
        public static void pickup(Tile target)
        {
            //Kollar så att inventarie inte är fullt.
            if (inventory.Contains(null))
            {
                //Lägger till objekt vid första lediga plats.
                int pos = 0;
                foreach (inventoryObject io in inventory)
                {
                    if (io == null)
                    {
                        //Lägger till objektet i inventarie.
                        inventory[pos] = target.interactable.convert();

                        //Tar bort objekt ifrån kartan.
                        target.removeTileObject();

                        break;
                    }

                    pos++;
                }
            }
        }

        //Rotera spelarens sprite åt olika håll.
        public static void Rotate(direction _direction)
        {
            //Sätter direction så att programmet vet vart spelaren tittar.
            facingDirection = _direction;

            //Återställer position på spelarens sprite.
            characterSprite = (Image)defaultSprite.Clone();

            //Gör en temporär bild.
            using (Bitmap drawSprite = new Bitmap(defaultSprite.Width, defaultSprite.Height))
            {
                //Sätter storleken på bilden.
                drawSprite.SetResolution(defaultSprite.HorizontalResolution, defaultSprite.VerticalResolution);

                //Genererar grafisk målare.
                using (Graphics g = Graphics.FromImage(drawSprite))
                {
                    //Flyttar bildens origo till mitten.
                    g.TranslateTransform((float)characterSprite.Width / 2, (float)characterSprite.Height / 2);

                    //Roterar bilden runt origo i grader åt rätt håll.
                    switch (facingDirection)
                    {
                        case direction.Right:
                            g.RotateTransform((int)direction.Right);
                            break;
                        case direction.Down:
                            g.RotateTransform((int)direction.Down);
                            break;
                        case direction.Left:
                            g.RotateTransform((int)direction.Left);
                            break;
                        case direction.Up:
                            g.RotateTransform((int)direction.Up);
                            break;
                    }
                    //Återställer origo till originalpositionen.
                    g.TranslateTransform(-(float)characterSprite.Width / 2, -(float)characterSprite.Height / 2);

                    //Ritar bilden på den roterade grafiken.
                    g.DrawImage(characterSprite, 0, 0);

                    //Kopierar den roterade bilden på spelarens sprite.
                    characterSprite = (Image)drawSprite.Clone();

                    //Gör av med den temporära grafikriotaren.
                    g.Dispose();
                }
                //Gör av med den temporära bilden.
                drawSprite.Dispose();
            }
        }

    }
}
